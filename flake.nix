{

description = "The flake for my NixOS system.";

inputs = {

nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";

home-manager = {
    url = "github:nix-community/home-manager/release-23.05";
    inputs.nixpkgs.follows = "nixpkgs";
};

nix-ld = {
    url = "github:Mic92/nix-ld";
    inputs.nixpkgs.follows = "nixpkgs";
};

ocaml-overlay = {
  url = "github:nix-ocaml/nix-overlays";
  inputs.nixpkgs.follows = "nixpkgs";
};

};

outputs = { self, nixpkgs, home-manager, nix-ld, emacs-overlay, ocaml-overlay, ... } @ inputs:

let
inherit (self) outputs;
system = "x86_64-linux";
lib = nixpkgs.lib;

overlays = [

inputs.emacs-overlay.overlays.default

inputs.ocaml-overlay.overlays.default

];

in {
nixosConfigurations = {

nixie = lib.nixosSystem {
inherit system;
# Whatever this is
specialArgs = { inherit inputs outputs; };

modules = [
  ./Nixie-Machine/configuration.nix
  { nixpkgs.overlays = overlays; }
  nix-ld.nixosModules.nix-ld
  home-manager.nixosModules.home-manager {
    home-manager.useGlobalPkgs = true;
    home-manager.useUserPackages = true;
    home-manager.users.astra = {
      imports = [ ./Nixie-Machine/Astra/astra.nix ];
    };
  }
];

};

};

};

}
